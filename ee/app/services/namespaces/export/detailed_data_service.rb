# frozen_string_literal: true

# TODO: this service is being implemented in https://gitlab.com/gitlab-org/gitlab/-/merge_requests/157296
module Namespaces
  module Export
    class DetailedDataService < BaseService
    end
  end
end
